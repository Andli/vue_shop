import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../components/Login.vue";
import Home from "../components/Home.vue";
import Users from "../components/users/Users.vue";
import Rights from "../components/rights/Rights.vue";
import Roles from "../components/rights/Roles.vue";
import Goods from "../components/shop/Goods.vue";
import Add from "../components/shop/Add.vue";
import Params from "../components/shop/Params.vue";
import Categories from "../components/shop/Categories.vue";
import Orders from "../components/order/Orders.vue";
import Welcome from "../components/Welcome.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/home",
    component: Home,
    redirect: '/welcome',
    children: [
      {
        path: "/welcome",
        component: Welcome,
      },
      {
        path: "/users",
        component: Users,
      },
      {
        path: "/rights",
        component: Rights,
      },
      {
        path: "/roles",
        component: Roles,
      },
      {
        path: "/goods",
        component: Goods,       
      },
      {
        path:'/goods/add',
        component: Add
      },
      {
        path:'/params',
        component: Params
      },
      {
        path:'/categories',
        component: Categories
      },
      {
        path:'/orders',
        component: Orders
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将访问哪一个路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数,表示放行
  //   next() 放行 next('/login') 强制跳转
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router;
